//
//  PinCodeGetController.h
//  Bonus
//
//  Created by Alex on 22.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@interface PinCodeGetController : BaseViewController

@property (strong, nonatomic) NSMutableDictionary *dataDictionary;
@property (strong, nonatomic) NSString *price;


@end
