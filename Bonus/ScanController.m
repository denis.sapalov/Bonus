//
//  ScanController.m
//  Bonus
//
//  Created by Alex on 16.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import "ScanController.h"
#import "MTBBarcodeScanner.h"
#import "BonusesController.h"
#import "Constants.h"
#import "LoginController.h"




@interface ScanController ()
@property (nonatomic, weak) IBOutlet UIView *previewView;
@property (nonatomic, weak) IBOutlet UIButton *toggleScanningButton;
@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (nonatomic, strong) MTBBarcodeScanner *scanner;
@property (nonatomic, strong) NSMutableArray *uniqueCodes;

@property (nonatomic, assign) BOOL captureIsFrozen;
@property (nonatomic, assign) BOOL didShowCaptureWarning;

@end

@implementation ScanController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Бонус";
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_arrow_back.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Выход" style:UIBarButtonItemStylePlain target:self action:@selector(exitAction)];

    // Do any additional setup after loading the view.
    [self customStileUI];
}

- (void) backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) exitAction {
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    [defaults setValue:@"" forKey:TOKEN_KEY];
    LoginController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
    [self presentViewController:vc animated:YES completion:nil];
}


#pragma mark - Lifecycle

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(previewTapped)];
    [self.previewView addGestureRecognizer:tapGesture];
    [self.toggleScanningButton addTarget:self action:@selector(toggleScanningTapped) forControlEvents:UIControlEventTouchDown];
    
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.scanner stopScanning];
    [super viewWillDisappear:animated];
}

- (void) customStileUI {
    self.mainView.layer.cornerRadius = 30.f;
    
    self.mainView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.mainView.layer.shadowOffset = CGSizeMake(0.f, 0.f);
    self.mainView.layer.shadowRadius = 2.f;
    self.mainView.layer.shadowOpacity = 2.f;
    self.mainView.layer.masksToBounds = NO;
    self.previewView.layer.cornerRadius = 28;
    UIBezierPath *maskPath = [UIBezierPath
                              bezierPathWithRoundedRect:self.toggleScanningButton.bounds
                              byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                              cornerRadii:CGSizeMake(30.f, 30.f)
                              ];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.toggleScanningButton.bounds;
    maskLayer.path = maskPath.CGPath;
    self.toggleScanningButton.layer.mask = maskLayer;
}

#pragma mark - Scanner

- (MTBBarcodeScanner *)scanner {
    if (!_scanner) {
        _scanner = [[MTBBarcodeScanner alloc] initWithPreviewView:_previewView];
    }
    return _scanner;
}

#pragma mark - Scanning

- (void)startScanning {
    self.uniqueCodes = [[NSMutableArray alloc] init];
    
    NSError *error = nil;
    [self.scanner startScanningWithResultBlock:^(NSArray *codes) {
        for (AVMetadataMachineReadableCodeObject *code in codes) {
            if (code.stringValue && [self.uniqueCodes indexOfObject:code.stringValue] == NSNotFound) {
                [self.uniqueCodes addObject:code.stringValue];
                
                NSLog(@"Found unique code: %@", code.stringValue);
                [self.scanner stopScanning];
                BonusesController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"BonusesController"];
                vc.barCodeFromScaner = code.stringValue;
                [self.navigationController pushViewController:vc animated:YES];

            }
        }
    } error:&error];
    
    if (error) {
        NSLog(@"An error occurred: %@", error.localizedDescription);
    }
    
    [self.toggleScanningButton setTitle:@"Остановить" forState:UIControlStateNormal];
    self.toggleScanningButton.backgroundColor = [UIColor redColor];
}

- (void)stopScanning {
    [self.scanner stopScanning];
    
    [self.toggleScanningButton setTitle:@"Сканировать" forState:UIControlStateNormal];
    self.toggleScanningButton.backgroundColor = self.view.tintColor;
    
    self.captureIsFrozen = NO;
}

#pragma mark - Actions

- (void)toggleScanningTapped {
    if ([self.scanner isScanning] || self.captureIsFrozen) {
        [self stopScanning];
    } else {
        [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
            if (success) {
                [self startScanning];
            } else {
                [self displayPermissionMissingAlert];
            }
        }];
    }
}

- (IBAction)switchCameraTapped:(id)sender {
    [self.scanner flipCamera];
}

- (void)backTapped {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Helper Methods

- (void)displayPermissionMissingAlert {
    NSString *message = nil;
    if ([MTBBarcodeScanner scanningIsProhibited]) {
        message = @"This app does not have permission to use the camera.";
    } else if (![MTBBarcodeScanner cameraIsPresent]) {
        message = @"This device does not have a camera.";
    } else {
        message = @"An unknown error occurred.";
    }
    
    [[[UIAlertView alloc] initWithTitle:@"Scanning Unavailable"
                                message:message
                               delegate:nil
                      cancelButtonTitle:@"Ok"
                      otherButtonTitles:nil] show];
}


#pragma mark - Gesture Handlers

- (void)previewTapped {
    if (![self.scanner isScanning] && !self.captureIsFrozen) {
        return;
    }
    
    if (!self.didShowCaptureWarning) {
        [[[UIAlertView alloc] initWithTitle:@"Capture Frozen"
                                    message:@"The capture is now frozen. Tap the preview again to unfreeze."
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil] show];
        
        self.didShowCaptureWarning = YES;
    }
    
    if (self.captureIsFrozen) {
        [self.scanner unfreezeCapture];
    } else {
        [self.scanner freezeCapture];
    }
    
    self.captureIsFrozen = !self.captureIsFrozen;
}

#pragma mark - Setters

- (void)setUniqueCodes:(NSMutableArray *)uniqueCodes {
    _uniqueCodes = uniqueCodes;
    //[self.tableView reloadData];
}






@end
