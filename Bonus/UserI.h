//
//  User.h
//  Bonus
//
//  Created by alex on 15.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserI : NSObject

@property (strong, nonatomic) NSString *token;
@property (strong, nonatomic) NSString *card;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *store;
@property (strong, nonatomic) NSString *surname;
@property (strong, nonatomic) NSString *type;

- (id) initWithServerResponse:(NSDictionary *)responseObject;

@end
