//
//  BonusesController.m
//  Bonus
//
//  Created by alex on 15.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import "BonusesController.h"
#import "CHRCardNumberMask.h"
#import "CHRTextFieldFormatter.h"
#import "ServerManager.h"
#import "Constants.h"
#import "BonusInfoController.h"
#import "ScanController.h"
#import "Discount.h"
#import "PurchaseWithPromoCodeController.h"
#import "PurchaseShopWithPromoCodeController.h"
#import "LoginController.h"





@interface BonusesController ()
@property (nonatomic, strong) CHRTextFieldFormatter *cardNumberFormatter;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;


@end

@implementation BonusesController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Бонус";

    self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicatorView.color = [UIColor blackColor];
    self.activityIndicatorView.center = self.view.center;
    self.activityIndicatorView.hidesWhenStopped = YES;
    [self.view addSubview:self.activityIndicatorView];

    
    [self customStileUI];
    self.cardNumberTextField.delegate = self;
    self.promoCodeTextField.delegate = self;
    
    if (self.barCodeFromScaner !=nil) {
        self.cardNumberTextField.text = self.barCodeFromScaner;
    }
    
   
    self.cardNumberFormatter = [[CHRTextFieldFormatter alloc] initWithTextField:self.cardNumberTextField mask:[CHRCardNumberMask new]];
    [self.serchButton addTarget:self action:@selector(pressSerchButton) forControlEvents:UIControlEventTouchDown];
    
    [self.scanButton addTarget:self action:@selector(scanButtonPressed) forControlEvents:UIControlEventTouchDown];
    
}



- (void) customStileUI {
    self.mainView.layer.cornerRadius = 30.f;
    self.mainView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.mainView.layer.shadowOffset = CGSizeMake(0.f, 0.f);
    self.mainView.layer.shadowRadius = 2.f;
    self.mainView.layer.shadowOpacity = 2.f;
    self.mainView.layer.masksToBounds = NO;
    self.scanButton.layer.cornerRadius = 8.f;
}

- (void) scanButtonPressed {
    ScanController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ScanController"];
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField*)textField {
    UIView *view = [self.view viewWithTag:textField.tag + 1];
    if (!view)
        [textField resignFirstResponder];
    else
        [view becomeFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.cardNumberTextField) {
        return [self.cardNumberFormatter textField:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    
    return YES;
}

- (void) pressSerchButton {
    [self.activityIndicatorView startAnimating];

    [self.cardNumberTextField resignFirstResponder];
    [self.promoCodeTextField resignFirstResponder];
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    NSString *token = [defaults valueForKey:TOKEN_KEY];
    
    if (self.promoCodeTextField.text.length < 1) {
        [[ServerManager sharedManager] bonusesGetWithToken:token
                                                      card:self.cardNumberTextField.text
                                                 onSuccess:^(NSDictionary* bonusDic) {
                                                     [self.activityIndicatorView stopAnimating];

                                                     BonusInfoController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"BonusInfoController"];
                                                     vc.dataDictionary = [NSMutableDictionary dictionary];
                                                     [vc.dataDictionary setDictionary:bonusDic];
                                                     [self.navigationController pushViewController:vc animated:YES];
                                                     
                                                 }
                                                    onFail:^(NSError *error) {
                                                        [self.activityIndicatorView stopAnimating];

                                                    }];

    } else {
        
        [[ServerManager sharedManager] discountGetWithToken:token
                                                       card:self.cardNumberTextField.text
                                                   discount:self.promoCodeTextField.text
                                                  onSuccess:^(id responData) {
                                                      [self.activityIndicatorView stopAnimating];

                                                      Discount *item = responData;
                                                      if ([item.errorCode isEqualToNumber:[NSNumber numberWithInt:0]] ) {
                                                          
                                                          if([[NSNumber numberWithInt:OPERATION_TYPE_PROMO]isEqual:item.type]) {
                                                              PurchaseWithPromoCodeController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PurchaseWithPromoCodeController"];
                                                              
                                                              vc.item = item;
                                                              [self.navigationController pushViewController:vc animated:YES];
                                                              
                                                          } else if([[NSNumber numberWithInt:OPERATION_TYPE_PROMO_SHOP]isEqual:item.type]) {
                                                              PurchaseShopWithPromoCodeController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PurchaseShopWithPromoCodeController"];
                                                              vc.item = item;
                                                              [self.navigationController pushViewController:vc animated:YES];
                                                          }
                                                      } else {
                                                          [self alertErrorWithMassage:item.errorText];
                                                      }
                                                  }
                                                     onFail:^(NSError *error) {
                                                         [self.activityIndicatorView stopAnimating];

                                                     }];
        
    }
    
    
    
   
    
}

- (void) alertErrorWithMassage: (NSString*) text {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:
                                @"" message:text preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * _Nonnull action) {}];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}



@end
