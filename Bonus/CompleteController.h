//
//  CompleteController.h
//  Bonus
//
//  Created by Alex on 30.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESTData.h"
#import "BaseViewController.h"


@interface CompleteController : BaseViewController
@property (strong, nonatomic) RESTData *APIData;

@end
