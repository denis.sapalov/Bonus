//
//  Utils.h
//  Bonus
//
//  Created by Denis on 7/12/17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

#define OPERATION_TYPE_USUAL 1
#define OPERATION_TYPE_PROMO 2
#define OPERATION_TYPE_PROMO_SHOP 3

@interface Utils : NSObject



+ (int) getOperationType:(NSString*)type;
@end
