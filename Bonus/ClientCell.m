//
//  ClientCell.m
//  Bonus
//
//  Created by Alex on 30.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import "ClientCell.h"

@implementation ClientCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
