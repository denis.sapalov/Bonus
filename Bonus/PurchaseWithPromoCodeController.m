//
//  PurchaseWithPromoCodeController.m
//  Bonus
//
//  Created by Alex on 31.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import "PurchaseWithPromoCodeController.h"
#import "ServerManager.h"
#import "UIImageView+AFNetworking.h"
#import "Constants.h"
#import "PromoPinEnterController.h"

@interface PurchaseWithPromoCodeController ()
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *discount;
@property (weak, nonatomic) IBOutlet UILabel *priceWithDiscount;
@property (weak, nonatomic) IBOutlet UILabel *card;
@property (weak, nonatomic) IBOutlet UILabel *bonusCount;
@property (weak, nonatomic) IBOutlet UILabel *goldCount;
@property (weak, nonatomic) IBOutlet UILabel *promoCode;
@property (weak, nonatomic) IBOutlet UITextField *useBonusesTextField;
@property (weak, nonatomic) IBOutlet UITextField *useGoldTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendButon;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;



@end

@implementation PurchaseWithPromoCodeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Бонус";
    self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicatorView.color = [UIColor blackColor];
    self.activityIndicatorView.center = self.view.center;
    self.activityIndicatorView.hidesWhenStopped = YES;
    [self.view addSubview:self.activityIndicatorView];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_arrow_back.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];

    
    [self customStileUI];
    self.price.text = [NSString stringWithFormat:@"Цена: %@ тг", self.item.price];
    self.discount.text = [NSString stringWithFormat:@"Скидка: %@%%", self.item.productDiscount];
    self.name.text = self.item.productName;
    self.priceWithDiscount.text = [NSString stringWithFormat:@"Цена со скидкой: %@ тг",self.item.productTotal];
    self.card.text = self.item.card;
    self.bonusCount.text = [NSString stringWithFormat:@"Количество бонусов: %@",self.item.userBonus];
    self.goldCount.text = [NSString stringWithFormat:@"Количество золота: %@",self.item.userGold];
    self.promoCode.text = self.item.discountCode;
    [self.imageView setImageWithURL:[NSURL URLWithString:self.item.productImageURL]];
    [self.sendButon addTarget:self action:@selector(pressButtonAction) forControlEvents:UIControlEventTouchDown];
    
    
}

- (void) backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField*)textField {
    UIView *view = [self.view viewWithTag:textField.tag + 1];
    if (!view)
        [textField resignFirstResponder];
    else
        [view becomeFirstResponder];
    return YES;
}

- (void) customStileUI {
    
    self.mainView.layer.cornerRadius = 30.f;
    self.mainView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.mainView.layer.shadowOffset = CGSizeMake(0.f, 0.f);
    self.mainView.layer.shadowRadius = 2.f;
    self.mainView.layer.shadowOpacity = 2.f;
    self.mainView.layer.masksToBounds = NO;
    self.sendButon.layer.cornerRadius = 8.f;
}

- (void) pressButtonAction {
    [self.activityIndicatorView startAnimating];

    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    NSString *token = [defaults valueForKey:TOKEN_KEY];
    
    [[ServerManager sharedManager] discountPinWithCardNumber:self.item.card
                                                      credit:[NSNumber numberWithInt:[self.useBonusesTextField.text intValue]]
                                                        gold:[NSNumber numberWithInt:[self.useGoldTextField.text intValue]]
                                                    discount:self.item.discountCode
                                                       token:token
                                                   onSuccess:^(id item) {
                                                       [self.activityIndicatorView stopAnimating];

                                                       Discount *result = item;
                                                       if ([result.errorCode isEqualToNumber:[NSNumber numberWithInt:0]]) {
                                                           PromoPinEnterController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PromoPinEnterController"];
                                                           vc.item = result;
                                                           [self.navigationController pushViewController:vc animated:YES];
                                                       } else {
                                                           [self alertErrorWithMassage:result.errorText];
                                                       }
                                                       
                                                   }
                                                      onFail:^(NSError *error) {
                                                          [self.activityIndicatorView stopAnimating];
                                                      }];
}

- (void) alertErrorWithMassage: (NSString*) text {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:
                                @"" message:text preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * _Nonnull action) {}];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
