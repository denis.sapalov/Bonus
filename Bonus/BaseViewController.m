//
//  BaseViewController.m
//  Bonus
//
//  Created by Alex on 31.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import "BaseViewController.h"
#import "LoginController.h"
#import "Constants.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Выход" style:UIBarButtonItemStylePlain target:self action:@selector(exitAction)];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) exitAction {
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    [defaults setValue:@"" forKey:TOKEN_KEY];
    LoginController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
    [self presentViewController:vc animated:YES completion:nil];
}

@end
