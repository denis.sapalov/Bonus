//
//  LoginController.m
//  Bonus
//
//  Created by alex on 15.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import "LoginController.h"
#import "ServerManager.h"
#import "Constants.h"
#import "UserI.h"
#import "BonusesController.h"
#import "ClientController.h"

@interface LoginController ()
@property (weak, nonatomic) IBOutlet UIView *loginView;
@property (weak, nonatomic) IBOutlet UITextField *liginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextfield;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;


@end

@implementation LoginController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicatorView.color = [UIColor blackColor];
    self.activityIndicatorView.center = self.view.center;
    self.activityIndicatorView.hidesWhenStopped = YES;
    [self.view addSubview:self.activityIndicatorView];
    
    [self customStileUI];
    
    [self.loginButton addTarget:self action:@selector(pressLoginButton) forControlEvents:UIControlEventTouchDown];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField*)textField {
    UIView *view = [self.view viewWithTag:textField.tag + 1];
    if (!view)
        [textField resignFirstResponder];
    else
        [view becomeFirstResponder];
    return YES;
}



- (void) customStileUI {
    
    self.loginView.layer.cornerRadius = 30.f;
    self.loginView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.loginView.layer.shadowOffset = CGSizeMake(0.f, 0.f);
    self.loginView.layer.shadowRadius = 2.f;
    self.loginView.layer.shadowOpacity = 2.f;
    self.loginView.layer.masksToBounds = NO;
    self.loginButton.layer.cornerRadius = 8.f;
}


- (void) pressLoginButton {
    [self.activityIndicatorView startAnimating];
    
    [self.liginTextField resignFirstResponder];
    [self.passwordTextfield resignFirstResponder];
    [[ServerManager sharedManager] userAuthWithEmail:self.liginTextField.text
                                         andPassword:self.passwordTextfield.text
                                           onSuccess:^(id user, NSNumber *errorCode, NSString *massage) {
                                               [self.activityIndicatorView stopAnimating];
                                               
                                               UserI *newUser = user;
                                               NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
                                               [defaults setValue:newUser.token forKey:TOKEN_KEY];
                                               
                                               if ([errorCode isEqualToNumber:[NSNumber numberWithInt:0]]) {
                                                   if ([newUser.type isEqualToString:@"Store"]) {
                                                       BonusesController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"BonusesController"];
                                                       UINavigationController *navVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MainNavigationController"];
                                                       navVC.navigationBar.barStyle = UIBarStyleBlack;
                                                       [navVC setViewControllers:@[vc] animated:NO];
                                                       [self presentViewController:navVC animated:YES completion:nil];
                                                   } else {
                                                       ClientController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ClientController"];
                                                       vc.user = newUser;
                                                       UINavigationController *navVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MainNavigationController"];
                                                       navVC.navigationBar.barStyle = UIBarStyleBlack;
                                                       [navVC setViewControllers:@[vc] animated:NO];
                                                       [self presentViewController:navVC animated:YES completion:nil];
                                                   }
                                               } else {
                                                   [self alertErrorWithMassage:massage];
                                               }
                                               
                                               
                                               
                                               
                                               
                                           }
                                              onFail:^(NSError *error) {
                                                  [self.activityIndicatorView stopAnimating];
                                              }];
}

- (void) alertErrorWithMassage: (NSString*) text {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:
                                @"" message:text preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * _Nonnull action) {}];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}


@end
