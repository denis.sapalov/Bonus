//
//  PinCodeGetController.m
//  Bonus
//
//  Created by Alex on 22.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import "PinCodeGetController.h"
#import "ServerManager.h"
#import "Constants.h"
#import "RESTData.h"
#import "CompleteController.h"


@interface PinCodeGetController ()

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *cardNumber;
@property (weak, nonatomic) IBOutlet UILabel *bonusCount;
@property (weak, nonatomic) IBOutlet UILabel *goldCount;
@property (weak, nonatomic) IBOutlet UILabel *sumBuying;
;
@property (weak, nonatomic) IBOutlet UILabel *usedGold;
@property (weak, nonatomic) IBOutlet UILabel *usedBonus;
@property (weak, nonatomic) IBOutlet UITextField *pinCodeTextField;

@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;


@end

@implementation PinCodeGetController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Бонус";
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_arrow_back.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];

    
    self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicatorView.color = [UIColor blackColor];
    self.activityIndicatorView.center = self.view.center;
    self.activityIndicatorView.hidesWhenStopped = YES;
    [self.view addSubview:self.activityIndicatorView];

    [self customStileUI];
    
    
    self.cardNumber.text = [[self.dataDictionary objectForKey:@"data"] objectForKey:@"card"];
    self.bonusCount.text =  [NSString stringWithFormat:@"Количество бонусов: %@",[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"user"] objectForKey:@"bonus"]];
    self.goldCount.text = [NSString stringWithFormat:@"Количество золота: %@", [[[self.dataDictionary objectForKey:@"data"] objectForKey:@"user"] objectForKey:@"gold"]];
    self.usedGold.text = [NSString stringWithFormat:@"Использовать бонусы: %@", [[[self.dataDictionary objectForKey:@"data"] objectForKey:@"request"]  objectForKey:@"gold"]];
    self.usedBonus.text = [NSString stringWithFormat:@"Использовать золото: %@", [[[self.dataDictionary objectForKey:@"data"] objectForKey:@"request"]  objectForKey:@"bonus"]];
    self.sumBuying.text = [NSString stringWithFormat:@"Сумма покупки: %@" , [[self.dataDictionary objectForKey:@"data"] objectForKey:@"price"]];
    
    [self.sendButton addTarget:self action:@selector(sendPinCode) forControlEvents:UIControlEventTouchDown];


    
}

- (void) backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) customStileUI {
    
    self.mainView.layer.cornerRadius = 30.f;
    self.mainView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.mainView.layer.shadowOffset = CGSizeMake(0.f, 0.f);
    self.mainView.layer.shadowRadius = 2.f;
    self.mainView.layer.shadowOpacity = 2.f;
    self.mainView.layer.masksToBounds = NO;
    self.sendButton.layer.cornerRadius = 8.f;
}


#pragma mark - API

- (void) sendPinCode {
    [self.activityIndicatorView startAnimating];

    [self.pinCodeTextField resignFirstResponder];
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    NSString *token = [defaults valueForKey:TOKEN_KEY];
    
    [[ServerManager sharedManager] bonusesCreditWithToken:token
                                               cardNumber:[[self.dataDictionary objectForKey:@"data"] objectForKey:@"card"]
                                                   credit:[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"request"]  objectForKey:@"bonus"]
                                                    price:[[self.dataDictionary objectForKey:@"data"] objectForKey:@"price"]
                                                     gold:[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"request"]  objectForKey:@"gold"]
                                                  pinCode:self.pinCodeTextField.text
                                                onSuccess:^(id responData) {
                                                    [self.activityIndicatorView stopAnimating];

                                                    RESTData *respData = responData;
                                                    if ([respData.errorCode isEqualToNumber:[NSNumber numberWithInt:0]]) {
                                                        NSLog(@"прошло успешно");
                                                        CompleteController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CompleteController"];
                                                        vc.APIData = respData;
                                                        [self.navigationController pushViewController:vc animated:YES];
                                                    } else {
                                                        [self.activityIndicatorView stopAnimating];
                                                        [self alertErrorWithMassage:respData.errorText];
                                                    }
                                                    
                                                }
                                                   onFail:^(NSError *error) {
                                                       
                                                   }];

    
}

- (void) alertErrorWithMassage: (NSString*) text {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:
                                @"" message:text preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * _Nonnull action) {}];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}



@end
