//
//  LoginController.h
//  Bonus
//
//  Created by alex on 15.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginController : UIViewController <UITextFieldDelegate>

@end
