//
//  CompleteController.m
//  Bonus
//
//  Created by Alex on 30.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import "CompleteController.h"
#import "ScanController.h"

@interface CompleteController ()

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *cardNumber;
@property (weak, nonatomic) IBOutlet UILabel *bonusCount;
@property (weak, nonatomic) IBOutlet UILabel *sumBuying;

@property (weak, nonatomic) IBOutlet UILabel *usedGold;
@property (weak, nonatomic) IBOutlet UILabel *usedBonus;

@property (weak, nonatomic) IBOutlet UIButton *sendButton;


@end

@implementation CompleteController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Бонус";
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_arrow_back.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.cardNumber.text = self.APIData.cardNumber;
    self.bonusCount.text =  [NSString stringWithFormat:@"Количество бонусов: %@", self.APIData.userBonus];
    self.usedGold.text = [NSString stringWithFormat:@"Использовано золота: %@", self.APIData.requestBonus];
    self.usedBonus.text = [NSString stringWithFormat:@"Использовано бонусов: %@", self.APIData.requestGold];
    self.sumBuying.text = [NSString stringWithFormat:@"Сумма покупки: %@" , self.APIData.price];
    
    [self.sendButton addTarget:self action:@selector(scanAction) forControlEvents:UIControlEventTouchDown];

    
}

- (void) backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) customStileUI {
    
    self.mainView.layer.cornerRadius = 30.f;
    self.mainView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.mainView.layer.shadowOffset = CGSizeMake(0.f, 0.f);
    self.mainView.layer.shadowRadius = 2.f;
    self.mainView.layer.shadowOpacity = 2.f;
    self.mainView.layer.masksToBounds = NO;
    self.sendButton.layer.cornerRadius = 8.f;
}

- (void) scanAction {
    ScanController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ScanController"];
    [self.navigationController pushViewController:vc animated:YES];
}


@end
