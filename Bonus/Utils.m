//
//  Utils.m
//  Bonus
//
//  Created by Denis on 7/12/17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (int) getOperationType:(NSString*)type {
    int result = 0;
    if([type isEqualToString:USUAL_PURCHASE]) {
        result = OPERATION_TYPE_USUAL;
    } else if([type isEqualToString:PROMO_PURCHASE]) {
        result = OPERATION_TYPE_PROMO;
    } else if([type isEqualToString:PROMO_SHOP_PURCHASE]) {
        result = OPERATION_TYPE_PROMO_SHOP;
    }

    return result;
}

@end
