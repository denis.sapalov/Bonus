//
//  BonusInfoController.m
//  Bonus
//
//  Created by alex on 16.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import "BonusInfoController.h"
#import "ServerManager.h"
#import "PinCodeGetController.h"
#import "Constants.h"

@interface BonusInfoController ()
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *cardNumber;
@property (weak, nonatomic) IBOutlet UILabel *bonusCount;
@property (weak, nonatomic) IBOutlet UILabel *goldCount;
@property (weak, nonatomic) IBOutlet UITextField *sumBuyingTextField
;
@property (weak, nonatomic) IBOutlet UITextField *useGoldTextfield;
@property (weak, nonatomic) IBOutlet UITextField *useBonusTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;


@end

@implementation BonusInfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Бонус";
    self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicatorView.color = [UIColor blackColor];
    self.activityIndicatorView.center = self.view.center;
    self.activityIndicatorView.hidesWhenStopped = YES;
    [self.view addSubview:self.activityIndicatorView];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_arrow_back.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];

    [self customStileUI];
    
    self.cardNumber.text = [[self.dataDictionary objectForKey:@"data"] objectForKey:@"card"];
    self.bonusCount.text = [NSString stringWithFormat:@"Количество бонусов: %@", [[[self.dataDictionary objectForKey:@"data"] objectForKey:@"user"] objectForKey:@"bonus"]];
    self.goldCount.text = [NSString stringWithFormat:@"Количество золота: %@", [[[self.dataDictionary objectForKey:@"data"] objectForKey:@"user"] objectForKey:@"gold"]];
    [self.sendButton addTarget:self action:@selector(sendButtonPressed) forControlEvents:UIControlEventTouchDown];
}

- (void) backAction {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void) customStileUI {
    
    self.mainView.layer.cornerRadius = 30.f;
    self.mainView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.mainView.layer.shadowOffset = CGSizeMake(0.f, 0.f);
    self.mainView.layer.shadowRadius = 2.f;
    self.mainView.layer.shadowOpacity = 2.f;
    self.mainView.layer.masksToBounds = NO;
    self.sendButton.layer.cornerRadius = 8.f;
}

#pragma mark - API

- (void) sendButtonPressed {
    [self.activityIndicatorView startAnimating];
    [self.useGoldTextfield resignFirstResponder];
    [self.useBonusTextField resignFirstResponder];
    [self.sumBuyingTextField resignFirstResponder];
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    NSString *token = [defaults valueForKey:TOKEN_KEY];
    
    [[ServerManager sharedManager] bonusesPinWithCardNumber:self.cardNumber.text
                                                     credit:[NSNumber numberWithInt:[self.useBonusTextField.text intValue]]
                                                       gold:[NSNumber numberWithInt:[self.useGoldTextfield.text intValue]]
                                                   andPrice:[NSNumber numberWithInt:[self.sumBuyingTextField.text intValue]]
                                                      token:token
                                                  onSuccess:^(NSDictionary *bonusDic) {
                                                      [self.activityIndicatorView stopAnimating];

                                                      NSNumber *result = [bonusDic objectForKey:@"result"];
                                                      if ([result isEqualToNumber:@1]) {
                                                          NSLog(@"Yahhhooooo!");
                                                          [self alertSendPinWithData:bonusDic];
                                                      } else {
                                                          
                                                          [self alertErrorWithMassage:[bonusDic objectForKey:@"error_text"]];
                                                      }
                                                      
                                                      
                                                  }
                                                     onFail:^(NSError *error) {
                                                         [self.activityIndicatorView stopAnimating];

                                                     }];
    
   }

- (void) alertErrorWithMassage: (NSString*) text {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:
                                @"" message:text preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * _Nonnull action) {}];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) alertSendPinWithData: (NSDictionary*) data {
    NSString *text = @"Пин-код был успешно выслан!";
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:
                                @"" message:text preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * _Nonnull action) {
                                                       PinCodeGetController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PinCodeGetController"];
                                                       vc.dataDictionary = (NSMutableDictionary*)data;
                                                       vc.price = self.sumBuyingTextField.text;
                                                       [self.navigationController pushViewController:vc animated:YES];
                                                   }];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}


@end
