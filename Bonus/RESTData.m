//
//  RESTData.m
//  Bonus
//
//  Created by Alex on 30.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import "RESTData.h"

@implementation RESTData

- (id) initWithServerResponse:(NSDictionary *)responseObject {
    self = [super init];
    if (self) {
        
        self.cardNumber = [[responseObject objectForKey:@"data"] objectForKey:@"card"];
        self.price = [[responseObject objectForKey:@"data"] objectForKey:@"price"];
        self.requestBonus = [[[responseObject objectForKey:@"data"] objectForKey:@"request"] objectForKey:@"bonus"];
        self.requestGold = [[[responseObject objectForKey:@"data"] objectForKey:@"request"] objectForKey:@"gold"];
        self.requestMaxBonus = [[[responseObject objectForKey:@"data"] objectForKey:@"request_max"] objectForKey:@"bonus"];
        self.requestMaxGold = [[[responseObject objectForKey:@"data"] objectForKey:@"request_max"] objectForKey:@"gold"];
        self.userBonus = [[[responseObject objectForKey:@"data"] objectForKey:@"user"] objectForKey:@"bonus"];
        self.userGold = [[[responseObject objectForKey:@"data"] objectForKey:@"user"] objectForKey:@"gold"];
        self.errorCode = [responseObject objectForKey:@"error"];
        self.token = [responseObject objectForKey:@"token"];
        self.errorText = [responseObject objectForKey:@"error_text"];
        
    }
    return self;
}


/*
 @property (strong, nonatomic) NSString *cardNumber;
 @property (strong, nonatomic) NSString *price;
 @property (strong, nonatomic) NSString *requestBonus;
 @property (strong, nonatomic) NSString *requestGold;
 @property (strong, nonatomic) NSString *requestMaxBonus;
 @property (strong, nonatomic) NSString *requestMaxGold;
 @property (strong, nonatomic) NSString *userBonus;
 @property (strong, nonatomic) NSString *userGold;
 @property (strong, nonatomic) NSNumber *errorCode;
 @property (strong, nonatomic) NSString *token;

 */

@end
