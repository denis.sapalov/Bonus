//
//  ServerManager.h
//  Bonus
//
//  Created by alex on 15.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface ServerManager : NSObject

+ (ServerManager*) sharedManager;

- (void) userAuthWithEmail: (NSString*) email
               andPassword: (NSString*) password
                 onSuccess:(void(^)(id user, NSNumber *errorCode, NSString *massage)) success
                    onFail:(void(^)(NSError* error)) failure;

- (void) bonusesGetWithToken: (NSString*) toke
                        card: (NSString*) card
                   onSuccess:(void(^)(NSDictionary* bonusDic)) success
                      onFail:(void(^)(NSError* error)) failure;

- (void) bonusesPinWithCardNumber: (NSString*) cardString
                           credit: (NSNumber*) credit
                             gold: (NSNumber*) gold
                         andPrice: (NSNumber*) price
                            token: (NSString*) tokenString
                        onSuccess:(void(^)(NSDictionary* bonusDic)) success
                           onFail:(void(^)(NSError* error)) failure;

- (void) bonusesCreditWithToken: (NSString *) tokenString
                     cardNumber: (NSString *) cardNumber
                         credit: (NSNumber *) credit
                          price: (NSNumber *) price
                           gold: (NSNumber *) gold
                        pinCode: (NSString *) pinCode
                      onSuccess:(void(^)(id responData)) success
                         onFail:(void(^)(NSError* error)) failure;

- (void) userGetWithToken: (NSString *) tokenString
               cardNumber: (NSString *) cardNumber
                onSuccess:(void(^)(id responData)) success
                   onFail:(void(^)(NSError* error)) failure;

- (void) discountGetWithToken: (NSString *) tokenString
                         card: (NSString *) cardNumber
                     discount: (NSString *) discountString
                    onSuccess:(void(^)(id responData)) success
                       onFail:(void(^)(NSError* error)) failure;

- (void) discountPinWithCardNumber: (NSString*) cardString
                            credit: (NSNumber*) credit
                              gold: (NSNumber*) gold
                          discount: (NSString*) discount
                             token: (NSString*) tokenString
                         onSuccess:(void(^)(id item)) success
                            onFail:(void(^)(NSError* error)) failure;
- (void) discountCreditWithToken: (NSString *) tokenString
                      cardNumber: (NSString *) cardNumber
                          credit: (NSNumber *) credit
                        discount: (NSString *) discount
                            gold: (NSNumber *) gold
                         pinCode: (NSString *) pinCode
                       onSuccess:(void(^)(id responData)) success
                          onFail:(void(^)(NSError* error)) failure;

@end
