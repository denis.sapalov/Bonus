//
//  PurchaseShopWithPromoCodeController.h
//  Bonus
//
//  Created by
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Discount.h"
#import "BaseViewController.h"


@interface PurchaseShopWithPromoCodeController : BaseViewController <UITextFieldDelegate>

@property (strong,  nonatomic) Discount *item;

@end
