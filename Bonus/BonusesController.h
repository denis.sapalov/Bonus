//
//  BonusesController.h
//  Bonus
//
//  Created by alex on 15.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface BonusesController : BaseViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIButton *scanButton;
@property (weak, nonatomic) IBOutlet UIButton *serchButton;
@property (weak, nonatomic) IBOutlet UITextField *cardNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *promoCodeTextField;

@property (strong, nonatomic) NSString* barCodeFromScaner;

@end
