//
//  BonusInfoController.h
//  Bonus
//
//  Created by alex on 16.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@interface BonusInfoController : BaseViewController

@property (strong, nonatomic) NSMutableDictionary *dataDictionary;

@end
