//
//  Discount.h
//  Bonus
//
//  Created by Alex on 31.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Discount : NSObject

@property (strong, nonatomic) NSString *card;
@property (strong, nonatomic) NSString *discountCode;
@property (strong, nonatomic) NSNumber *price;

@property (strong, nonatomic) NSNumber *productBonus;
@property (strong, nonatomic) NSNumber *productDiscount;
@property (strong, nonatomic) NSNumber *productId;
@property (strong, nonatomic) NSString *productImageURL;
@property (strong, nonatomic) NSString *productName;
@property (strong, nonatomic) NSNumber *productPrice;
@property (strong, nonatomic) NSNumber *productTotal;

@property (strong, nonatomic) NSNumber *requestBonus;
@property (strong, nonatomic) NSNumber *requestGold;

@property (strong, nonatomic) NSNumber *requestMaxBonus;
@property (strong, nonatomic) NSNumber *requestMaxGold;

@property (strong, nonatomic) NSNumber *userBonus;
@property (strong, nonatomic) NSNumber *userGold;

@property (strong, nonatomic) NSNumber *errorCode;
@property (strong, nonatomic) NSString *tokenString;
@property (strong, nonatomic) NSString *errorText;
@property (strong, nonatomic) NSNumber *type;

@property (strong, nonatomic) NSNumber *total;

// company
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *logo;
@property (strong, nonatomic) NSNumber *cashback;
@property (strong, nonatomic) NSNumber *discount;

- (id) initWithServerResponse:(NSDictionary *)responseObject;

@end
