//
//  User.m
//  Bonus
//
//  Created by alex on 15.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import "UserI.h"

@implementation UserI

- (id) initWithServerResponse:(NSDictionary *)responseObject {
    self = [super init];
    if (self) {
        self.token = [responseObject objectForKey:@"token"];
        self.card = [[responseObject objectForKey:@"user"] objectForKey:@"card"];
        self.email = [[responseObject objectForKey:@"user"] objectForKey:@"email"];
        self.name = [[responseObject objectForKey:@"user"] objectForKey:@"name"];
        self.store = [[responseObject objectForKey:@"user"] objectForKey:@"store"];
        self.surname = [[responseObject objectForKey:@"user"] objectForKey:@"surname"];
        self.type = [[responseObject objectForKey:@"user"] objectForKey:@"type"];
    }
    return self;
}
@end
