//
//  Discount.m
//  Bonus
//
//  Created by Alex on 31.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import "Discount.h"

@implementation Discount

- (id) initWithServerResponse:(NSDictionary *)responseObject {
    self = [super init];
    if (self) {
        NSDictionary *company = [[responseObject objectForKey: @"data"] objectForKey:@"company"];
        if(company) {
            self.name = [company objectForKey:@"name"];
            self.logo = [company objectForKey:@"logo"];
            self.cashback = [company objectForKey:@"cashback"];
            self.discount = [company objectForKey:@"discount"];
        }
        
        self.card = [[responseObject objectForKey: @"data"] objectForKey:@"card"];
        self.discountCode = [[responseObject objectForKey:@"data"] objectForKey:@"discount_code"];
        self.type = [[responseObject objectForKey:@"data"] objectForKey:@"type"];
        self.price = [[responseObject objectForKey:@"data"] objectForKey:@"price"];
        
        self.productBonus = [[[responseObject objectForKey:@"data"] objectForKey:@"product"] objectForKey:@"bonus"];
        self.productDiscount = [[[responseObject objectForKey:@"data"] objectForKey:@"product"] objectForKey:@"discount"];
        self.productId = [[[responseObject objectForKey:@"data"] objectForKey:@"product"] objectForKey:@"id"];
        self.productImageURL = [[[responseObject objectForKey:@"data"] objectForKey:@"product"] objectForKey:@"img"];
        self.productName = [[[responseObject objectForKey:@"data"] objectForKey:@"product"] objectForKey:@"name"];
        self.productPrice = [[[responseObject objectForKey:@"data"] objectForKey:@"product"] objectForKey:@"price"];
        self.productTotal = [[[responseObject objectForKey:@"data"] objectForKey:@"product"] objectForKey:@"total"];
        
        self.requestBonus = [[[responseObject objectForKey:@"data"] objectForKey:@"request"] objectForKey:@"bonus"];
        self.requestGold = [[[responseObject objectForKey:@"data"] objectForKey:@"request"] objectForKey:@"gold"];
        
        self.requestMaxBonus = [[[responseObject objectForKey:@"data"] objectForKey:@"request_max"] objectForKey:@"bonus"];
        self.requestMaxGold = [[[responseObject objectForKey:@"data"] objectForKey:@"request_max"] objectForKey:@"gold"];
        
        self.userBonus = [[[responseObject objectForKey:@"data"] objectForKey:@"user"] objectForKey:@"bonus"];
        self.userGold = [[[responseObject objectForKey:@"data"] objectForKey:@"user"] objectForKey:@"gold"];
        
        self.errorCode = [responseObject objectForKey:@"error"];
        self.tokenString = [responseObject objectForKey:@"token"];
        self.errorText = [responseObject objectForKey:@"error_text"];
    }
    return self;
}


@end

