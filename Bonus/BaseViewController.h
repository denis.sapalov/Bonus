//
//  BaseViewController.h
//  Bonus
//
//  Created by Alex on 31.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

- (void) exitAction;

@end
