//
//  PromoCompleteController.m
//  Bonus
//
//  Created by Alex on 31.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import "PromoCompleteController.h"
#import "UIImageView+AFNetworking.h"
#import "ScanController.h"

@interface PromoCompleteController ()
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *discount;
@property (weak, nonatomic) IBOutlet UILabel *card;
@property (weak, nonatomic) IBOutlet UILabel *bonusCount;
@property (weak, nonatomic) IBOutlet UILabel *goldCount;
@property (weak, nonatomic) IBOutlet UILabel *bonusUsed;
@property (weak, nonatomic) IBOutlet UILabel *goldUsed;
@property (weak, nonatomic) IBOutlet UILabel *promoCode;
@property (weak, nonatomic) IBOutlet UIButton *sendButon;
@property (weak, nonatomic) IBOutlet UILabel *name;
@end

@implementation PromoCompleteController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Бонус";
    
    [self customStileUI];
    self.price.text = [NSString stringWithFormat:@"Цена: %@ тг", self.item.price];
    self.discount.text = [NSString stringWithFormat:@"Скидка: %@%%", self.item.productDiscount];
    self.name.text = self.item.productName;
    self.card.text = self.item.card;
    self.bonusCount.text = [NSString stringWithFormat:@"Количество бонусов: %@",self.item.userBonus];
    self.goldCount.text = [NSString stringWithFormat:@"Количество золота: %@",self.item.userGold];
    self.bonusUsed.text = [NSString stringWithFormat:@"Использовано бонусов: %@", self.item.requestBonus];
    self.goldUsed.text = [NSString stringWithFormat:@"Использовано золота: %@", self.item.requestGold];
    
    self.promoCode.text = self.item.discountCode;
    [self.imageView setImageWithURL:[NSURL URLWithString:self.item.productImageURL]];
    [self.sendButon addTarget:self action:@selector(pressButtonAction) forControlEvents:UIControlEventTouchDown];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_arrow_back.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
}

- (void) pressButtonAction {
    ScanController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ScanController"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) customStileUI {
    self.mainView.layer.cornerRadius = 30.f;
    self.mainView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.mainView.layer.shadowOffset = CGSizeMake(0.f, 0.f);
    self.mainView.layer.shadowRadius = 2.f;
    self.mainView.layer.shadowOpacity = 2.f;
    self.mainView.layer.masksToBounds = NO;
    self.sendButon.layer.cornerRadius = 8.f;
}


@end
