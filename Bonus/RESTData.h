//
//  RESTData.h
//  Bonus
//
//  Created by Alex on 30.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RESTData : NSObject

@property (strong, nonatomic) NSString *cardNumber;
@property (strong, nonatomic) NSString *price;
@property (strong, nonatomic) NSString *requestBonus;
@property (strong, nonatomic) NSString *requestGold;
@property (strong, nonatomic) NSString *requestMaxBonus;
@property (strong, nonatomic) NSString *requestMaxGold;
@property (strong, nonatomic) NSString *userBonus;
@property (strong, nonatomic) NSString *userGold;
@property (strong, nonatomic) NSNumber *errorCode;
@property (strong, nonatomic) NSString *token;
@property (strong, nonatomic) NSString *errorText;

- (id) initWithServerResponse:(NSDictionary *)responseObject;


@end
