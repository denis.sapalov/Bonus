//
//  Constants.h
//  Bonus
//
//  Created by alex on 15.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#define TOKEN_KEY @"tokenKey"
#define UDID_KEY @"udidKey"


#define USUAL_PURCHASE @"USUAL_URCHASE"
#define PROMO_PURCHASE @"PROMO_PURCHASE"
#define PROMO_SHOP_PURCHASE @"PROMO_SHOP_PURCHASE"

#define OPERATION_TYPE_USUAL  1
#define OPERATION_TYPE_PROMO  2
#define OPERATION_TYPE_PROMO_SHOP 3
