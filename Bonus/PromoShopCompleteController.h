//
//  PromoShopCompleteController.h
//  Bonus
//
//  Created by 
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Discount.h"
#import "BaseViewController.h"


@interface PromoShopCompleteController : BaseViewController
@property (strong, nonatomic) Discount *item;


@end
