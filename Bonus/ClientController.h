//
//  ClientController.h
//  Bonus
//
//  Created by Alex on 30.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserI.h"
#import "BaseViewController.h"
#import "RSCodeGen.h"


@interface ClientController : BaseViewController <UITableViewDelegate, UITableViewDataSource>
{
    UIView *barCodeView;
}

@property (strong, nonatomic) UserI *user;

@end
