//
//  ClientController.m
//  Bonus
//
//  Created by Alex on 30.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import "ClientController.h"
#import "ServerManager.h"
#import "Constants.h"
#import "DataModels.h"
#import "ClientCell.h"


#define BARCODE_SIZE  300

@import FirebaseInstanceID;
@import FirebaseMessaging;

@interface ClientController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (strong ,nonatomic) NSMutableArray *companyArray;
@property (weak, nonatomic) IBOutlet UIButton *scanButton;
@property (weak, nonatomic) IBOutlet UIButton *genBarCodeButton;
@property (weak, nonatomic) IBOutlet UIButton *linkButton;
@property (weak, nonatomic) IBOutlet UILabel *cardNumber;
@property (weak, nonatomic) IBOutlet UILabel *pinCode;
@property (weak, nonatomic) IBOutlet UILabel *bonusCount;
@property (weak, nonatomic) IBOutlet UILabel *goldCount;
@property (strong, nonatomic) BaseClass *itemBase;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;


@end

@implementation ClientController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Бонус";
    self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicatorView.color = [UIColor blackColor];
    self.activityIndicatorView.center = self.view.center;
    self.activityIndicatorView.hidesWhenStopped = YES;
    [self.view addSubview:self.activityIndicatorView];
    [self.activityIndicatorView startAnimating];
    self.companyArray = [NSMutableArray array];
    [self getUserInfo];
    [self customStileUI];
    
    [self.genBarCodeButton addTarget:self action:@selector(showBarCode) forControlEvents:UIControlEventTouchDown];
    [self.scanButton addTarget:self action:@selector(getUserInfo) forControlEvents:UIControlEventTouchDown];
    [self.linkButton addTarget:self action:@selector(linkButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.companyArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ClientCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ClientCell"];
    if (!cell) {
        cell = [[ClientCell alloc]  initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ClientCell"];
    }
    
    Companies *item = [self.companyArray objectAtIndex:indexPath.row];
    List *list = [item.list objectAtIndex:0];
    
    cell.mainText.text = [NSString stringWithFormat:@"%@ - %@", list.name, item.total]; //list.name;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 26.f;
}



#pragma mark - API

- (void) getUserInfo {
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    NSString *token = [defaults valueForKey:TOKEN_KEY];
    
    [[ServerManager sharedManager] userGetWithToken:token
                                         cardNumber:self.user.card
                                          onSuccess:^(id responData) {
                                              [self.activityIndicatorView stopAnimating];
                                              BaseClass *model = responData;
                                              self.itemBase = model;
                                              [self.companyArray removeAllObjects];
                                              [self.companyArray addObjectsFromArray:model.data.companies];
                                              self.cardNumber.text = self.itemBase.data.card;
                                              self.bonusCount.text = [NSString stringWithFormat:@"Бонусов: %@", self.itemBase.data.user.bonus];
                                              self.goldCount.text = [NSString stringWithFormat:@"Золота: %d", (int)self.itemBase.data.user.gold];
                                              [self.tableView reloadData];
                                          }
                                             onFail:^(NSError *error) {
                                                 [self.activityIndicatorView stopAnimating];

                                             }];

    
    
}

- (void) customStileUI {
    
    UIBezierPath *maskPath = [UIBezierPath
                              bezierPathWithRoundedRect:self.footerView.bounds
                              byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                              cornerRadii:CGSizeMake(30.f, 30.f)
                              ];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.footerView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.footerView.layer.mask = maskLayer;
    
    UIBezierPath *maskPathH = [UIBezierPath
                              bezierPathWithRoundedRect:self.headerView.bounds
                              byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight)
                              cornerRadii:CGSizeMake(30.f, 30.f)
                              ];
    
    CAShapeLayer *maskLayerH = [CAShapeLayer layer];
    maskLayerH.frame = self.headerView.bounds;
    maskLayerH.path = maskPathH.CGPath;
    self.headerView.layer.mask = maskLayerH;
    
    self.scanButton.layer.cornerRadius = 8.f;
    self.genBarCodeButton.layer.cornerRadius = 8.f;
}

- (void) linkButtonPressed {
    [[UIApplication sharedApplication]
     openURL:[NSURL URLWithString: @"http://goldmoney.kz/"]];
}

- (void)handleLogTokenTouch {
    // [START get_iid_token]
    NSString *token = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", token);
    // [END get_iid_token]
}

- (UIImage *) generateBarCode {
    return [CodeGen genCodeWithContents:self.itemBase.data.card machineReadableCodeObjectType:AVMetadataObjectTypeCode93Code];
}

- (void) showBarCode {
    
    barCodeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, BARCODE_SIZE, BARCODE_SIZE)];
    barCodeView.center = self.view.center;
    barCodeView.backgroundColor = [UIColor redColor];
    
    UIImageView *imv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, BARCODE_SIZE, BARCODE_SIZE)];
    [imv setImage:[self generateBarCode]];
    
    [barCodeView addSubview:imv];
    [self.view addSubview:barCodeView];
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch= [touches anyObject];
    if ([touch view] == barCodeView)
    {
        [barCodeView removeFromSuperview];
    }
    
}





@end
