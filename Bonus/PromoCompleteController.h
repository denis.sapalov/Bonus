//
//  PromoCompleteController.h
//  Bonus
//
//  Created by Alex on 31.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Discount.h"
#import "BaseViewController.h"


@interface PromoCompleteController : BaseViewController
@property (strong, nonatomic) Discount *item;


@end
