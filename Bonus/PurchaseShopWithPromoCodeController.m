//
//  PurchaseShopWithPromoCodeController.m
//  Bonus
//
//  Created by
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import "PurchaseShopWithPromoCodeController.h"
#import "ServerManager.h"
#import "UIImageView+AFNetworking.h"
#import "Constants.h"
#import "PromoShopPinEnterController.h"

#define KB_FHIFT 110

@interface PurchaseShopWithPromoCodeController ()
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *discount;
@property (weak, nonatomic) IBOutlet UILabel *priceWithDiscount;
@property (weak, nonatomic) IBOutlet UILabel *card;
@property (weak, nonatomic) IBOutlet UILabel *bonusCount;
@property (weak, nonatomic) IBOutlet UILabel *goldCount;
@property (weak, nonatomic) IBOutlet UILabel *promoCode;
@property (weak, nonatomic) IBOutlet UITextField *useBonusesTextField;
@property (weak, nonatomic) IBOutlet UITextField *useGoldTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendButon;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UITextField *sumBuyingTextField;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;



@end

@implementation PurchaseShopWithPromoCodeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Бонус";
    self.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicatorView.color = [UIColor blackColor];
    self.activityIndicatorView.center = self.view.center;
    self.activityIndicatorView.hidesWhenStopped = YES;
    [self.view addSubview:self.activityIndicatorView];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_arrow_back.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];

    
    [self customStileUI];
    self.price.text = [NSString stringWithFormat:@"Цена: %@ тг", self.item.price];
    self.discount.text = [NSString stringWithFormat:@"Скидка: %@%%", self.item.discount];
    self.name.text = self.item.name;
    self.priceWithDiscount.text = [NSString stringWithFormat:@"Цена со скидкой: %@ тг",self.item.productTotal];
    self.card.text = self.item.card;
    self.bonusCount.text = [NSString stringWithFormat:@"Количество бонусов: %@",self.item.userBonus];
    self.goldCount.text = [NSString stringWithFormat:@"Количество золота: %@",self.item.userGold];
    self.promoCode.text = self.item.discountCode;
    [self.imageView setImageWithURL:[NSURL URLWithString:self.item.logo]];
    [self.sendButon addTarget:self action:@selector(pressButtonAction) forControlEvents:UIControlEventTouchDown];
    
    
}

- (void) backAction {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void) customStileUI {
    
    self.mainView.layer.cornerRadius = 30.f;
    self.mainView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.mainView.layer.shadowOffset = CGSizeMake(0.f, 0.f);
    self.mainView.layer.shadowRadius = 2.f;
    self.mainView.layer.shadowOpacity = 2.f;
    self.mainView.layer.masksToBounds = NO;
    self.sendButon.layer.cornerRadius = 8.f;
}

- (void) pressButtonAction {
    [self.activityIndicatorView startAnimating];

    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    NSString *token = [defaults valueForKey:TOKEN_KEY];
    
    [[ServerManager sharedManager] bonusesPinWithCardNumber:self.item.card
                                                     credit:[NSNumber numberWithInt:[self.useBonusesTextField.text intValue]]
                                                       gold:[NSNumber numberWithInt:[self.useGoldTextField.text intValue]]
                                                   andPrice:[NSNumber numberWithInt:[self.sumBuyingTextField.text intValue]]
                                                      token:token
                                                  onSuccess:^(NSDictionary *bonusDic) {
                                                      
                                                      [self.activityIndicatorView stopAnimating];
                                                      
                                                      NSNumber *result = [bonusDic objectForKey:@"result"];
                                                      if ([result isEqualToNumber:@1]) {
                                                          PromoShopPinEnterController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PromoShopPinEnterController"];
                                                          
                                                          Discount *resultItem = [[Discount alloc] initWithServerResponse:bonusDic];
                                                          resultItem.price = [NSNumber numberWithInt:[self.sumBuyingTextField.text intValue]];
                                                          resultItem.discount = self.item.discount;
                                                          resultItem.discountCode = self.item.discountCode;
                                                          resultItem.userBonus = [NSNumber numberWithInt:[self.useBonusesTextField.text intValue]];
                                                          resultItem.userGold = [NSNumber numberWithInt:[self.useGoldTextField.text intValue]];
                                                          
                                                          vc.item = resultItem;
                                                          [self.navigationController pushViewController:vc animated:YES];

                                                      } else {
                                                          
                                                          [self alertErrorWithMassage:[bonusDic objectForKey:@"error_text"]];
                                                      }
                                                  }
                                                     onFail:^(NSError *error) {
                                                         [self.activityIndicatorView stopAnimating];
                                                         
                                                     }];
}

- (BOOL)textFieldShouldReturn:(UITextField*)textField {
    UIView *view = [self.view viewWithTag:textField.tag + 1];
    if (!view)
        [textField resignFirstResponder];
    else
        [view becomeFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.mainView endEditing:YES];
    return YES;
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    [self.mainView setFrame:CGRectMake(self.mainView.frame.origin.x, 0 - KB_FHIFT, self.mainView.frame.size.width, self.mainView.frame.size.height)];
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.mainView setFrame:CGRectMake(self.mainView.frame.origin.x, KB_FHIFT, self.mainView.frame.size.width, self.mainView.frame.size.height)];
}


- (void) alertErrorWithMassage: (NSString*) text {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:
                                @"" message:text preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * _Nonnull action) {}];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
