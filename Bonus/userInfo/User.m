//
//  User.m
//
//  Created by   on 30.01.17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "User.h"


NSString *const kUserGold = @"gold";
NSString *const kUserBonus = @"bonus";
NSString *const kUserSurname = @"surname";
NSString *const kUserName = @"name";


@interface User ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation User

@synthesize gold = _gold;
@synthesize bonus = _bonus;
@synthesize surname = _surname;
@synthesize name = _name;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.gold = [[self objectOrNilForKey:kUserGold fromDictionary:dict] doubleValue];
            self.bonus = [self objectOrNilForKey:kUserBonus fromDictionary:dict];
            self.surname = [self objectOrNilForKey:kUserSurname fromDictionary:dict];
            self.name = [self objectOrNilForKey:kUserName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.gold] forKey:kUserGold];
    [mutableDict setValue:self.bonus forKey:kUserBonus];
    [mutableDict setValue:self.surname forKey:kUserSurname];
    [mutableDict setValue:self.name forKey:kUserName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.gold = [aDecoder decodeDoubleForKey:kUserGold];
    self.bonus = [aDecoder decodeObjectForKey:kUserBonus];
    self.surname = [aDecoder decodeObjectForKey:kUserSurname];
    self.name = [aDecoder decodeObjectForKey:kUserName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_gold forKey:kUserGold];
    [aCoder encodeObject:_bonus forKey:kUserBonus];
    [aCoder encodeObject:_surname forKey:kUserSurname];
    [aCoder encodeObject:_name forKey:kUserName];
}

- (id)copyWithZone:(NSZone *)zone {
    User *copy = [[User alloc] init];
    
    
    
    if (copy) {

        copy.gold = self.gold;
        copy.bonus = [self.bonus copyWithZone:zone];
        copy.surname = [self.surname copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
    }
    
    return copy;
}


@end
