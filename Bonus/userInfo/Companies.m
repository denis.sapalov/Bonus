//
//  Companies.m
//
//  Created by   on 30.01.17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Companies.h"
#import "List.h"


NSString *const kCompaniesId = @"id";
NSString *const kCompaniesTotal = @"total";
NSString *const kCompaniesList = @"list";


@interface Companies ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Companies

@synthesize companiesIdentifier = _companiesIdentifier;
@synthesize total = _total;
@synthesize list = _list;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.companiesIdentifier = [self objectOrNilForKey:kCompaniesId fromDictionary:dict];
            self.total = [self objectOrNilForKey:kCompaniesTotal fromDictionary:dict];
    NSObject *receivedList = [dict objectForKey:kCompaniesList];
    NSMutableArray *parsedList = [NSMutableArray array];
    
    if ([receivedList isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedList addObject:[List modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedList isKindOfClass:[NSDictionary class]]) {
       [parsedList addObject:[List modelObjectWithDictionary:(NSDictionary *)receivedList]];
    }

    self.list = [NSArray arrayWithArray:parsedList];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.companiesIdentifier forKey:kCompaniesId];
    [mutableDict setValue:self.total forKey:kCompaniesTotal];
    NSMutableArray *tempArrayForList = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.list) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForList addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForList addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForList] forKey:kCompaniesList];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.companiesIdentifier = [aDecoder decodeObjectForKey:kCompaniesId];
    self.total = [aDecoder decodeObjectForKey:kCompaniesTotal];
    self.list = [aDecoder decodeObjectForKey:kCompaniesList];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_companiesIdentifier forKey:kCompaniesId];
    [aCoder encodeObject:_total forKey:kCompaniesTotal];
    [aCoder encodeObject:_list forKey:kCompaniesList];
}

- (id)copyWithZone:(NSZone *)zone {
    Companies *copy = [[Companies alloc] init];
    
    
    
    if (copy) {

        copy.companiesIdentifier = [self.companiesIdentifier copyWithZone:zone];
        copy.total = [self.total copyWithZone:zone];
        copy.list = [self.list copyWithZone:zone];
    }
    
    return copy;
}


@end
