//
//  Data.h
//
//  Created by   on 30.01.17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class User;

@interface Data : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *companies;
@property (nonatomic, strong) NSString *card;
@property (nonatomic, strong) User *user;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
