//
//  Companies.h
//
//  Created by   on 30.01.17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Companies : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *companiesIdentifier;
@property (nonatomic, strong) NSString *total;
@property (nonatomic, strong) NSArray *list;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
