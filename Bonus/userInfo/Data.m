//
//  Data.m
//
//  Created by   on 30.01.17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Data.h"
#import "Companies.h"
#import "User.h"


NSString *const kDataCompanies = @"companies";
NSString *const kDataCard = @"card";
NSString *const kDataUser = @"user";


@interface Data ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Data

@synthesize companies = _companies;
@synthesize card = _card;
@synthesize user = _user;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedCompanies = [dict objectForKey:kDataCompanies];
    NSMutableArray *parsedCompanies = [NSMutableArray array];
    
    if ([receivedCompanies isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedCompanies) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedCompanies addObject:[Companies modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedCompanies isKindOfClass:[NSDictionary class]]) {
       [parsedCompanies addObject:[Companies modelObjectWithDictionary:(NSDictionary *)receivedCompanies]];
    }

    self.companies = [NSArray arrayWithArray:parsedCompanies];
            self.card = [self objectOrNilForKey:kDataCard fromDictionary:dict];
            self.user = [User modelObjectWithDictionary:[dict objectForKey:kDataUser]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForCompanies = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.companies) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForCompanies addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForCompanies addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForCompanies] forKey:kDataCompanies];
    [mutableDict setValue:self.card forKey:kDataCard];
    [mutableDict setValue:[self.user dictionaryRepresentation] forKey:kDataUser];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.companies = [aDecoder decodeObjectForKey:kDataCompanies];
    self.card = [aDecoder decodeObjectForKey:kDataCard];
    self.user = [aDecoder decodeObjectForKey:kDataUser];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_companies forKey:kDataCompanies];
    [aCoder encodeObject:_card forKey:kDataCard];
    [aCoder encodeObject:_user forKey:kDataUser];
}

- (id)copyWithZone:(NSZone *)zone {
    Data *copy = [[Data alloc] init];
    
    
    
    if (copy) {

        copy.companies = [self.companies copyWithZone:zone];
        copy.card = [self.card copyWithZone:zone];
        copy.user = [self.user copyWithZone:zone];
    }
    
    return copy;
}


@end
