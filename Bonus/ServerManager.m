//
//  ServerManager.m
//  Bonus
//
//  Created by alex on 15.01.17.
//  Copyright © 2017 Asta.Mobi. All rights reserved.
//

#import "ServerManager.h"
#import <CommonCrypto/CommonDigest.h>
#import "UserI.h"
#import "RESTData.h"
#import "BaseClass.h"
#import "Discount.h"
#import "Constants.h"
@import FirebaseInstanceID;
@import FirebaseMessaging;

@interface ServerManager ()
@property (strong, nonatomic) AFHTTPSessionManager *sessionManager;
@end

@implementation ServerManager

+ (ServerManager*) sharedManager {
    
    static ServerManager* manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ServerManager alloc] init];
    });
    
    return manager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSURL *url = [NSURL URLWithString:@"http://goldmoney.kz/api/"];
        
        self.sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
    }
    return self;
}

- (NSString *) generateMD5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest );
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
}

- (void)handleLogTokenTouch {
    // [START get_iid_token]
    
    NSString *token = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", token);
    // [END get_iid_token]
}

#pragma mark - API methods

- (void) userAuthWithEmail: (NSString*) email
               andPassword: (NSString*) password
                 onSuccess:(void(^)(id user, NSNumber *errorCode, NSString *massage)) success
                    onFail:(void(^)(NSError* error)) failure;{
    
    NSString *hash = [self generateMD5:[NSString stringWithFormat:@"%@%@", email, [self generateMD5:password]]];
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    NSString *uid = [defaults valueForKey:UDID_KEY];
    NSLog(@"push uid - > %@", uid);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:email, @"email", hash, @"hash", uid, @"uid",nil];
    
    [self.sessionManager POST:@"user_auth"
                   parameters:params
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                          NSLog(@"userAuth responseObject %@", responseObject);
                          UserI *user = [[UserI alloc] initWithServerResponse:responseObject];
                          NSString *errorText = [responseObject objectForKey:@"error_text"];
                          NSNumber *errorCode = [responseObject objectForKey:@"error"];
                          if (success) {
                              success(user, errorCode, errorText);
                          }
                      }
                      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          if (failure) {
                              failure (error);
                          }
                      }];
}

- (void) bonusesGetWithToken: (NSString*) token
                        card: (NSString*) card
                   onSuccess:(void(^)(NSDictionary* bonusDic)) success
                      onFail:(void(^)(NSError* error)) failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:token, @"token", card, @"card", nil];
    
    [self.sessionManager GET:@"i_get"
                  parameters:params
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         //NSLog(@"bonusesGet responseObject %@", responseObject);
                         if (success) {
                             NSNumber* errorCode = [responseObject objectForKey:@"error"];
                             if ([errorCode isEqualToNumber:[NSNumber numberWithInt:0]]) {
                                 success(responseObject);
                             }
                         }
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         if (failure) {
                             failure (error);
                         }
                     }];
}

- (void) bonusesPinWithCardNumber: (NSString*) cardString
                           credit: (NSNumber*) credit
                             gold: (NSNumber*) gold
                         andPrice: (NSNumber*) price
                            token: (NSString*) tokenString
                        onSuccess:(void(^)(NSDictionary* bonusDic)) success
                           onFail:(void(^)(NSError* error)) failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:tokenString, @"token", cardString, @"card", credit ,@"credit", price, @"price", gold, @"gold", nil];
    
    [self.sessionManager GET:@"i_pin"
                  parameters:params
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         NSLog(@"bonusesPin responseObject %@", responseObject);
                         NSNumber *result = [responseObject objectForKey: @"result"];
                         NSLog(@"result %@", result);
                         if (success) {
                             success(responseObject);
                         }
                         
                     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         if (failure) {
                             failure (error);
                         }
                     }];
}

- (void) bonusesCreditWithToken: (NSString *) tokenString
                     cardNumber: (NSString *) cardNumber
                         credit: (NSNumber *) credit
                          price: (NSNumber *) price
                           gold: (NSNumber *) gold
                        pinCode: (NSString *) pinCode
                      onSuccess:(void(^)(id responData)) success
                         onFail:(void(^)(NSError* error)) failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:tokenString, @"token",
                            cardNumber, @"card",
                            credit ,@"credit",
                            price, @"price",
                            gold, @"gold",
                            pinCode, @"pin_code", nil];
    
    [self.sessionManager GET:@"i_credit"
                  parameters:params
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         NSLog(@"bonuses_credit responseObject %@", responseObject);
                         RESTData *responData = [[RESTData alloc] initWithServerResponse:responseObject];
                         if (success) {
                             success(responData);
                         }
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         if (failure) {
                             failure (error);
                         }
                     }];
}

- (void) userGetWithToken: (NSString *) tokenString
               cardNumber: (NSString *) cardNumber
                onSuccess:(void(^)(id responData)) success
                   onFail:(void(^)(NSError* error)) failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:tokenString, @"token", cardNumber, @"card", nil];
    
    [self.sessionManager GET:@"user_get"
                  parameters:params
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         NSLog(@"user_get responseObject %@", responseObject);
                         BaseClass *user = [[BaseClass alloc] initWithDictionary:responseObject];
                         if (user.error == 0) {
                             if (success) {
                                 success (user);
                             }
                         }
                         
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         if (failure) {
                             failure (error);
                         }
                     }];
}


- (void) discountGetWithToken: (NSString *) tokenString
                         card: (NSString *) cardNumber
                     discount: (NSString *) discountString
                    onSuccess:(void(^)(id responData)) success
                       onFail:(void(^)(NSError* error)) failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:tokenString, @"token", cardNumber, @"card", discountString, @"discount", nil];
    
    
    
    [self.sessionManager GET:@"i_get"
                  parameters:params
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         NSLog(@"discount_get -> %@", responseObject);
                         Discount *item = [[Discount alloc] initWithServerResponse:responseObject];
                         if (success) {
                             success (item);
                         }
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         if (failure) {
                             failure (error);
                         }
                     }];
}

- (void) discountPinWithCardNumber: (NSString*) cardString
                           credit: (NSNumber*) credit
                             gold: (NSNumber*) gold
                         discount: (NSString*) discount
                            token: (NSString*) tokenString
                        onSuccess:(void(^)(id item)) success
                           onFail:(void(^)(NSError* error)) failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:tokenString, @"token", cardString, @"card", credit ,@"credit", discount, @"discount", gold, @"gold", nil];
    
    [self.sessionManager GET:@"i_pin"
                  parameters:params
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         NSLog(@"bonusesPin responseObject %@", responseObject);
                         Discount *item = [[Discount alloc] initWithServerResponse:responseObject];
                         if (success) {
                             success (item);
                         }
                         
                     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         if (failure) {
                             failure (error);
                         }
                     }];
}

- (void) discountCreditWithToken: (NSString *) tokenString
                     cardNumber: (NSString *) cardNumber
                         credit: (NSNumber *) credit
                        discount: (NSString *) discount
                           gold: (NSNumber *) gold
                        pinCode: (NSString *) pinCode
                      onSuccess:(void(^)(id responData)) success
                         onFail:(void(^)(NSError* error)) failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:tokenString, @"token",
                            cardNumber, @"card",
                            credit ,@"credit",
                            discount, @"discount",
                            gold, @"gold",
                            pinCode, @"pin_code", nil];
    
    [self.sessionManager GET:@"i_credit"
                  parameters:params
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         NSLog(@"bonuses_credit responseObject %@", responseObject);
                         Discount *item = [[Discount alloc] initWithServerResponse:responseObject];
                         if (success) {
                             success (item);
                         }
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         if (failure) {
                             failure (error);
                         }
                     }];
}

@end
